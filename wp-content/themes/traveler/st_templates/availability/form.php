<?php

wp_enqueue_script( 'bootstrap-datepicker.js' ); 

wp_enqueue_script( 'bootstrap-datepicker-lang.js' );

wp_enqueue_script( 'new-calendar', get_template_directory_uri() . '/js/new_availability.js', ['jquery'], NULL, TRUE);

wp_enqueue_script( 'new-calendar-mod', get_template_directory_uri() . '/js/new_availability_mod.js', ['jquery'], NULL, TRUE);



$post_id = intval($_GET['id']); 

$date = STInput::get('date', date('d/m/Y'). '-'. date('d/m/Y', strtotime('+1 day')));

global $post;

$hotel = new STHotel();

$st_fetch_rooms = $hotel->st_fetch_rooms($post_id);



$next_periode_id = $hotel->sf_fetch_periodes($post_id);

if(!$next_periode_id){

    $next_periode_id = 1;

} else {

    $next_periode_id += 1;

}

$liste_periodes = $hotel->sf_fetch_periodes_rows($post_id);



?>



<span class="hidden st_partner_avaiablity <?php echo STInput::get('sc') ?> "></span>

<div class="row calendar-wrapper template-user" data-post-id="<?php echo esc_html($post_id); ?>">

    <div class="col-xs-12 col-md-12 calendar-wrapper-inner st-partner-field-item">

        <h5 class="st-group-heading">Périodes & tarifs</h5>

        <?php if(!empty($liste_periodes)): ?>

            

        <table class="periode_table table table-bordered table-striped table-hover">

        <thead>

            <tr>

                <th>Période</th> 

                <th>Date</th> 

                <th>Disponibilité</th> 

                <th>Saison</th> 

                <th colspan="2">Actions</th> 

            </tr>

        </thead>

            <?php foreach($liste_periodes as $periode): ?>

                <tr id="<?php echo "periode-" . $periode->id ?>" class="single-periode <?php echo "periode_".$periode->saison . " periode_".$periode->status;  ?>">

                    <td class="td1"><p>Période <?php echo $periode->periode_id . " (PID " . $periode->id . ")"; ?></p></td>

                    <td class="td2"><p>Du <b><?php echo date("m/d/Y", $periode->periode_from) ?></b> Au <b><?php echo date("m/d/Y", $periode->periode_to) ?></b></p></td>

                    <td class="td3"><p><?php echo $hotel->sf_print_status($periode->status) ?></p></td>

                    <td class="td4"><p><?php echo $hotel->sf_print_periode_saison($periode->saison) ?></p></td>

                    <td class="td5">

                        <a  data-toggle="modal" 

                            data-target="#info-periode-modal" 

                            class="btn btn-xs btn-primary mt5 btn-info-periode" 

                            data-post_id="<?php echo esc_html($post_id); ?>" 

                            data-periode_id="<?php echo $periode->id ?>" 

                            href="javascript: void(0);"><span class="hidden-xs">Modifier</span></a>

                    </td>

                    <td class="td6">

                        <input type="hidden" name="del_post_id" value="<?php echo esc_html($post_id); ?>">

                        <input type="hidden" name="del_periode_id" value="<?php echo $periode->id ?>">

                        <input type="button" value="Supprimer" class="delete_periode btn btn-xs btn-primary mt5"/> 

                    </td>

                </tr>

            <?php endforeach; ?>    

        </table>

        <?php endif; ?>

        <div class="modal fade modal-cancel-booking modal-info-periode" id="info-periode-modal" tabindex="-1" role="dialog"

            aria-labelledby="cancelBookingLabel">

            <div class="modal-dialog modal-md" role="document">

                <div class="modal-content">

                    <div class="modal-header">

                        <button type="button" class="close" data-dismiss="modal"

                                aria-label="<?php echo __('Close', ST_TEXTDOMAIN); ?>"><span aria-hidden="true">&times;</span>

                        </button>

                        <h4 class="modal-title" id="cancelBookingLabel"><?php echo __('Modifier periode', ST_TEXTDOMAIN); ?></h4>

                    </div>

                    <div class="modal-body">

                        <div style="display: none;" class="overlay-form"><i class="fa fa-spinner text-color"></i></div>

                        <div class="modal-content-inner"></div>

                    </div>

                </div>

            </div>

        </div>

        <h5 class="st-group-heading">Ajouter une nouvelle période</h5>

        <div class="st-field-group sf_periode_bloc">

            <div class="row">

                <div class="col-lg-6 st-partner-field-item">

                    <div class="form-group st-field-select">

                        <label for="st-field-sf_saison">Saison</label>

                        <select id="st-field-sf_saison" name="sf_saison" class="st-partner-field form-control" data-condition="" data-operator="or">

                            <option value="-1" selected="">Sélectionnez</option>

                            <option value="basse_saison">Basse saison</option>

                            <option value="moyenne_saison">Moyenne Saison</option>

                            <option value="haute_saison">Haute Saison</option>

                        </select>

                    </div>

                </div>

                <div class="col-lg-6 st-partner-field-item">

                    <div class="form-group st-field-text">

                        <label for="st-field-sf_periode">Période du .. au ..</label>

                        <div class="form-group form-date-field form-date-search clearfix has-icon" data-format="<?php echo TravelHelper::getDateFormatMoment() ?>">

                            <input type="text" required id="sf_periode" name="sf_periode" class="st-partner-field form-control" value="<?php echo $date; ?>" />

                        </div>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-lg-3 st-partner-field-item">

                    <div class="form-group st-field-text">

                        <label for="st-field-sf_disponiblite">Disponibilité</label>

                        <select id="st-field-sf_disponiblite" name="sf_disponiblite" class="st-partner-field form-control" data-condition="" data-operator="or">

                            <option value="-1" selected="">Sélectionnez</option>

                            <option value="available">Disponible</option>

                            <option value="unavailable">Complet</option>

                        </select>

                    </div>

                </div>

                <div class="col-lg-3 st-partner-field-item">

                    <div class="form-group st-field-text">

                        <label for="st-field-sf_retrocession">Délai de retrocession (-jour)</label>

                        <input type="number" min="0" name="sf_retrocession" id="sf_retrocession" class="form-control" placeholder="Delai de rétrocession" required>

                    </div>

                </div>

                <div class="col-lg-3 st-partner-field-item">

                    <div class="form-group st-field-text">

                        <label for="st-field-sf_calendar_marge_price">Marge tarif</label>

                        <input type="number" min="0" max="100" name="sf_calendar_marge_price" id="sf_calendar_marge_price" class="form-control" placeholder="Valeur en %" required>

                    </div>

                </div>

                <div class="col-lg-3 st-partner-field-item">

                    <div class="form-group st-field-text">

                        <label for="st-field-sf_calendar_promotion_prix">Promotion</label>

                        <input type="number" min="0" max="100" name="sf_calendar_promotion_prix" id="sf_calendar_promotion_prix" class="form-control" placeholder="Valeur en %" required>

                    </div>

                </div>

            </div>

            <hr>

            <div class="row st-partner-field-item">

            <div class="col-lg-12 st-partner-field-item"><h5 class="st-group-heading">Prix arrangement(s)</h5></div>

                <?php

                $facilities = get_the_terms($post_id, 'hotel-accommodation');

                    if ($facilities) {

                    $count = count($facilities);

                ?>   

                    <?php                              

                    if (isset($facilities) && is_array($facilities)) {

                        foreach ($facilities as $term) {

                    ?>

                        <div class="col-lg-4 form-group st-field-text block">

                            <label for="st-field-sf_periode"> <?php echo "Prix en <b>" . esc_html($term->name) . "</b> (RID: ".$term->term_id.")"; ?></label>

                            <input type="hidden" id ="acc" name="sf_pension_accommodation_id[]" value="<?php echo esc_html($term->term_id); ?>">

                            <input type="number" min="0" max="100"  data-value='<?php echo esc_html($term->term_id); ?>'  name="sf_pension_accommodation[]" value="" class="form-control" placeholder="Prix en DT" required></label> 

                        </div>

                        <?php } // end foreach ?>

                    <?php } // end if facilities ?>

                <?php } // end facilities  ?>	

            </div>

            <div class="row st-partner-field-item">

                <?php                   

                $facilities = get_the_terms($post_id, 'extra');

                    if ($facilities) {

                    $count = count($facilities);

                ?>   

                    <?php                              

                    if (isset($facilities) && is_array($facilities)) {

                        foreach ($facilities as $term) {

                    ?>

                        <div class="col-lg-4 form-group st-field-text block">

                            <label for="st-field-sf_periode"> <?php echo "Supplément <b>" . esc_html($term->name) . "</b>"; ?></label>

                            <input type="hidden" id ="extra" name="sf_extra_id[]" value="<?php echo esc_html($term->term_id); ?>">

                            <input type="number" min="0" max="100"  data-value='<?php echo esc_html($term->term_id); ?>'  name="sf_extra[]" value="" class="form-control" placeholder="Prix en DT" required></label> 

                        </div>

                        <?php } // end foreach ?>

                    <?php } // end if facilities ?> 			

                <?php } // end facilities  ?>	

            </div>

            <div class="row">

            <div class="col-lg-12 st-partner-field-item"><h5 class="st-group-heading">Stock/chambre(s)</h5></div>

            <?php

                $rooms = get_saved_rooms($post_id);

                if ($rooms) {

                    foreach ($rooms as $room) {

            ?>

                    <div class="col-lg-4 form-group st-field-text block">

                        <label for="st-field-sf_stock"> <?php echo "Stock <b>" . get_the_title($room) . "</b>"; ?></label>

                        <input type="hidden" id ="stock" name="sf_stock_id[]" value="<?= $room ?>">

                        <input type="number" min="0" max="100"  data-value='<?= $room; ?>'  name="sf_stock[]" value="" class="form-control" placeholder="Stock chambre" required></label> 

                    </div>

                    <?php } // end foreach rooms  ?>

            <?php } // end rooms  ?>

            </div>

            <hr>

            <div class="row">

            <div class="col-lg-12 st-partner-field-item"><h5 class="st-group-heading">Réductions</h5></div>

                <div class="col-lg-4 st-partner-field-item">

                    <div class="form-group st-field-select">

                        <label for="st-field-sf_prix_single">Supplément Single</label>

                        <input type="number" min="0" max="100" class="st-partner-field form-control" value="" name="sf_prix_single" placeholder="Valeur en DT" required>

                    </div>

                </div>

                <div class="col-lg-4 st-partner-field-item">

                    <div class="form-group st-field-text">

                        <label for="st-field-sf_trois_lits">Réduction 3éme & 4éme lit</label>

                        <input type="number" min="0" max="100" class="st-partner-field form-control" value="" name="sf_trois_lits" placeholder="Valeur en %" required>

                    </div>

                </div>

                <div class="col-lg-4 st-partner-field-item">

                    <div class="form-group st-field-text">

                        <label for="st-field-sf_min_stay">Minimum stay</label>

                        <input type="number" min="0" max="100" class="st-partner-field form-control" value="" name="sf_min_stay" placeholder="Nombre de jour" required>

                    </div>

                </div>

                <div class="col-lg-12 st-partner-field-item">

                    <div class="form-group st-field-select">

                        <label for="st-field-sf_msg_promotion">Message promotionnel</label>

                        <input type="text" class="st-partner-field form-control" value="" name="msg_promotion" placeholder="Votre message de promotion ici">

                    </div>

                </div>

            </div>

            

            

            <?php if($st_fetch_rooms) :  $array_age = array();?>

            <div class="row sf_adult">

                <div class="col-lg-12 st-partner-field-item"><h5 class="st-group-heading">Réduction Adulte(s)/Enfant(s)</h5></div>

                <div class="col-lg-12 st-partner-field-item">

                    <div class="table-responsive">

                        <table class="table table-bordered table-striped table-hover">

                            <thead>

                                <tr>

                                    <th rowspan="2">Avec Adulte</th>

                                        <?php for ($i = 1; $i <= 2; $i++){ ?>

                                    

                                        <?php 

                                            if (get_post_meta( $post_id, 'min_age_hot_'.$i, true ) > 0 && get_post_meta( $post_id, 'max_age_hot_'.$i, true ) > 0) { 

                                                $array_age[$i] = get_post_meta( $post_id, 'min_age_hot_'.$i, true ) . '_' . get_post_meta( $post_id, 'max_age_hot_'.$i, true );

                                            ?>

                                        <th colspan="3"><?php echo __('Prix enfant', ST_TEXTDOMAIN); ?> <strong><?php echo (get_post_meta( $post_id, 'min_age_hot_'.$i, true ) ) ;?></strong>

                                            <?php echo __(' et ', ST_TEXTDOMAIN); ?> 

                                            <strong><?php echo (get_post_meta( $post_id, 'max_age_hot_'.$i, true ) ) ;?> </strong>						

                                            <input type="hidden"  name="<?php echo 'min_age_'.$i; ?>" value="<?php echo (get_post_meta( $post_id, 'min_age_hot_'.$i, true ) ) ;?>" >

                                            <input type="hidden"  name="<?php echo 'max_age_'.$i; ?>" value="<?php echo (get_post_meta( $post_id, 'max_age_hot_'.$i, true ) ) ;?>" >

                                        </th>

                                    <?php  } } ?>	

                                </tr>

                                <tr>

                                    <?php if (get_post_meta( $post_id, 'min_age_hot_1', true ) > 0 && get_post_meta( $post_id, 'max_age_hot_1', true ) > 0) { ?>

                                    <?php for ($i = 1; $i <= 3; $i++){ ?>

                                        <th>Avec <?php echo $i; ?>&nbsp;enfant(s)</th>

                                    <?php  }} ?>

                                        <?php if (get_post_meta( $post_id, 'min_age_hot_2', true ) > 0 && get_post_meta( $post_id, 'max_age_hot_2', true ) > 0) { ?>

                                        <?php for ($j = 1; $j <= 3; $j++){ ?>

                                            <th>Avec <?php echo $j; ?>&nbsp;enfant(s)</th>

                                    <?php  }} ?>

                                </tr>

        

                            </thead>

                            <tbody>

                                <?php for ($r = 0; $r < $st_fetch_rooms; $r++){ ?>

                                            <tr>

                                                <td>

                                                    <?php echo __($r.' Adulte', ST_TEXTDOMAIN); ?>

                                                    <input type="hidden"  name="adult_number" value="<?php echo $r ;?>" >

                                                </td>

                                                <?php for ($i = 1; $i <= 2; $i++){ ?>

                                                    <?php if (get_post_meta( $post_id, 'min_age_hot_'.$i, true ) > 0 && get_post_meta( $post_id, 'max_age_hot_'.$i, true ) > 0) { ?>

                                                        <?php for ($j = 1; $j <= 3; $j++){ ?>

                                                            <td><input style="font-size: 11px" type="number" min="0" max="100" name="<?php echo 'adult_enfant['. $r .'][age-'.$array_age[$i].'-enf-'.$j.'-adult-'.$r .']' ?> " id="<?php echo (get_post_meta( $post_id, 'age-'.$array_age[$i].'-enf-'.$j.'-adult-'.$r, true ) ) ;?>" value="" class="form-control" placeholder="<?php echo __('age-'.$array_age[$i].'-enf-'.$j.'-adult-'.$r, ST_TEXTDOMAIN); ?>"></td>

                                                <?php } } } ?>

                                            </tr>



                                <?php } ?>    

                            </tbody>

                        </table>

                    </div>

                </div>

            </div>

            <?php endif; ?>

            <div class="row">

                <div class="col-lg-12">

                    <input type="hidden" name="calendar_check_in" id="calendar_check_in" value="">

                    <input type="hidden" name="calendar_check_out" id="calendar_check_out" value="">

                    <input type="hidden" name="calendar_post_id" value="<?php echo esc_html($post_id); ?>">

                    <input type="hidden" name="periode_id" value="<?php echo $next_periode_id; ?>">

                    <input type="button" value="Ajouter une nouvelle période" class="add_periode"/>

                </div>

            </div>

            <div class="overlay__"></div>

        </div>

    </div>

    <?php do_action('traveler_after_form_hotel_calendar'); ?>

</div>