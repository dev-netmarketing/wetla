<?php
    /**
     * Created by PhpStorm.
     * User: Administrator
     * Date: 19-11-2018
     * Time: 8:56 AM
     * Since: 1.0.0
     * Updated: 1.0.0
     */
	 
    //$room_id   = get_the_ID();
    $room_id = $data['post_id'];

    //  $extra_price = get_post_meta( $room_id, 'extra_price', true );
    $extra_price = $data['extraPrice'];

    $extra = STInput::get( 'extra' );
    if ( empty( $extra ) ) {
        $extra = [];
    }

    if ( !empty( $extra[ 'value' ] ) ) {
        $extra_value = $extra[ 'value' ];
    }
	
    //dd($data);


	$accommodation = STInput::get( 'accommodation' );
?>
<div class="form-group form-more-extra">
    <ul class="extras" style="display:block !important">
	
        <?php foreach ( $extra_price as $key => $val ):
				?>
                <li  class="col-md-6 item mt10">
                    <div class="st-flex space-between">
                        <span><?php echo esc_html($val['title']); ?>(<?php echo TravelHelper::format_money( $val[ 'extra_price' ] ) ?>) <span class="c-orange">*</span> </span>
                        <div class="select-wrapper" style="width: 50px;">
                            <?php
                                $max_item = intval( $val[ 'extra_max_number' ] );
                                if ( $max_item <= 0 ) $max_item = 1;
                            ?>
                            <select class="form-control app extra-service-select"
                                    name="extra_price<?php echo $data['j'];?>[value][<?php echo esc_attr($val[ 'extra_name' ]); ?>]"
                                    id="field-<?php echo esc_attr($val[ 'extra_name' ]); ?>"
                                    data-extra-price="<?php echo esc_attr($val[ 'extra_price' ]); ?>">
                                <?php
                                    $max_item = intval( $val[ 'extra_max_number' ] );
                                    if ( $max_item <= 0 ) $max_item = 1;
                                    $start_i = 0;
                                    if ( isset( $val[ 'extra_required' ] ) ) {
                                        if ( $val[ 'extra_required' ] == 'on' ) {
                                            $start_i = 1;
                                        }
                                    }
                                    for ( $i = $start_i; $i <= $max_item; $i++ ):
                                        $check = "";
                                        if ( !empty( $extra_value[ $val[ 'extra_name' ] ] ) and $i == $extra_value[ $val[ 'extra_name' ] ] ) {
                                            $check = "selected";
                                        }
                                        ?>
                                        <option <?php echo esc_html( $check ) ?>
                                                value="<?php echo esc_attr($i); ?>"><?php echo esc_html($i); ?></option>
                                    <?php endfor; ?>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="extra_price<?php echo $data['j'];?>[price][<?php echo esc_attr( $val[ 'extra_name' ]); ?>]"
                           value="<?php echo esc_attr($val[ 'extra_price' ]); ?>">
                    <input type="hidden" name="extra_price<?php echo $data['j'];?>[title][<?php echo esc_attr($val[ 'extra_name' ]); ?>]"
                           value="<?php echo esc_attr($val[ 'title' ]); ?>">
                </li>
        <?php endforeach; ?>
		
		<?php
		
		$post_id  = get_post_meta( get_the_ID(), 'room_parent', true );
		$facilities = get_the_terms($room_id, 'hotel-accommodation');

		if (isset($facilities) && is_array($facilities)) {	?>
            <li class=" col-md-6 item mt10">
                <div class="st-flex space-between">
                    <span>Arrangement</span>
                    <div class="select-wrapper">
                        <select class="form-control app extra-service-select" name="accommodation<?php echo $data['j'];?>" >
                            <?php foreach ($facilities as $term) { ?>
                                <option value="<?php echo esc_html($term->term_id) ; ?>"><?php echo esc_html($term->name) ; ?></option>
                            <?php }   ?>
                        </select>

                    </div>
                </div>
            </li>
		<?php }   ?>
    </ul>

 </div>
