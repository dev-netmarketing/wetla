<?php
$result_page = st()->get_option( 'hotel_search_result_page' );
$class = '';
$id = 'id="sticky-nav"';
if(isset($in_tab)) {
    $class = 'in_tab';
    $id = '';
}
$has_icon        = ( isset( $has_icon ) ) ? $has_icon : false;
?>
<!--homepage search form!-->
<div class="search-form hotel-search-form-home hotel-search-form search-form-here <?php echo esc_attr($class); ?>" <?php echo ($id); ?>>
    <form action="<?php echo esc_url( get_the_permalink( $result_page ) ); ?>" class="form" method="get">
        <div class="row">
            <div class="col-md-4 border-right">
                <?php echo st()->load_template( 'layouts/modern/hotel/elements/search/location', '', [ 'has_icon' => true ] ) ?>
            </div>
            <div class="col-md-3 border-right">
                <?php echo st()->load_template( 'layouts/modern/hotel/elements/search/date', '', [ 'has_icon' => true ] ) ?>
            </div>
            <div class="col-md-2 border-right guest-select search_fullbar">
                <?php echo st()->load_template( 'layouts/modern/hotel/elements/search/guest-chambre', '', [ 'has_icon' => true ] ) ?>
            </div>
            <div class="col-md-3">
                <?php echo st()->load_template( 'layouts/modern/hotel/elements/search/advanced', '' ) ?>
            </div>
        </div>
        <div class="row search_rooms">
            <div class="col-md-12">
                <?php
                // Search bar homepage
                echo st()->load_template( 'layouts/modern/hotel/elements/search/guest', '', [ 'has_icon' => false ] ) 
                ?>
            </div>
        </div>
    </form>
</div>
