<?php
	$user_id = get_current_user_id();
	$order_item = get_post($order_code);
	wp_reset_postdata();
	$payment_method = get_post_meta($order_item->ID, 'payment_method', true);
	$currency = get_post_meta($order_code, 'currency', true);
?>
<div class="row booking-success-notice">
	<div class="col-lg-8 col-md-8 col-left">
		<img src="<?php echo get_template_directory_uri(); ?>/v2/images/ico_success.svg" alt="Payment Success"/>
		<div class="notice-success">
			<p class="line1"><span><?php echo get_post_meta($order_code, 'st_first_name', true); ?>,</span>
				<?php
				if (get_post_meta($order_code, 'payment_method', true) == 'st_submit_form') {
					echo __('Votre résérvation a été bien recue !' , ST_TEXTDOMAIN ) ;
				} else
					echo __('Votre commande a été bien validée !' , ST_TEXTDOMAIN ) ;
				?>
			</p>
			<p class="line2"><?php _e('Les détails de votre commande ont été envoyés à: ',ST_TEXTDOMAIN); ?><span><?php echo get_post_meta($order_code, 'st_email', true) ?></span></p>
		</div>
	</div>
	<div class="col-lg-4 col-md-4">
		<ul class="booking-info-detail">
			<li><span><?php echo __('Numéro de la résérvation:', ST_TEXTDOMAIN); ?></span> <?php echo esc_html($order_code) ?></li>
			<li><span><?php echo __('Date:', ST_TEXTDOMAIN); ?></span> <?php echo get_the_time(TravelHelper::getDateFormat(), $order_code) ?></li>
			<li><span><?php echo __('Méthode de paiement:', ST_TEXTDOMAIN); ?></span> <?php //echo STPaymentGateways::get_gatewayname(get_post_meta($order_code, 'payment_method', true)); ?>payer à l'agence</li>
		</ul>
	</div>
</div>
<div class="row">
	
	<div class="col-lg-12 col-md-12">
		<h3 class="title">
			<?php echo __('Vos informations', ST_TEXTDOMAIN); ?>
		</h3>
        <?php
        ob_start();
        ?>
		<div class="info-form">
			<ul>
				<li><span class="label"><?php echo __('First Name', ST_TEXTDOMAIN); ?></span><span class="value"><?php echo get_post_meta($order_code, 'st_first_name', true) ?></span></li>
				<li><span class="label"><?php echo __('Last name', ST_TEXTDOMAIN); ?></span><span class="value"><?php echo get_post_meta($order_code, 'st_last_name', true) ?></span></li>
				<li><span class="label"><?php echo __('Email', ST_TEXTDOMAIN); ?></span><span class="value"><?php echo get_post_meta($order_code, 'st_email', true) ?></span></li>
				<li><span class="label"><?php echo __('Address Line 1', ST_TEXTDOMAIN); ?></span><span class="value"><?php echo get_post_meta($order_code, 'st_address', true) ?></span></li>
				<li><span class="label"><?php echo __('Address Line 2', ST_TEXTDOMAIN); ?></span><span class="value"><?php echo get_post_meta($order_code, 'st_address2', true) ?></span></li>
				<li><span class="label"><?php echo __('City', ST_TEXTDOMAIN); ?></span><span class="value"><?php echo get_post_meta($order_code, 'st_city', true) ?></span></li>
				<li><span class="label"><?php echo __('State/Province/Region', ST_TEXTDOMAIN); ?></span><span class="value"><?php echo get_post_meta($order_code, 'st_province', true) ?></span></li>
				<li><span class="label"><?php echo __('ZIP code/Postal code', ST_TEXTDOMAIN); ?></span><span class="value"><?php echo get_post_meta($order_code, 'st_zip_code', true) ?></span></li>
				<li><span class="label"><?php echo __('Country', ST_TEXTDOMAIN); ?></span><span class="value"><?php echo get_post_meta($order_code, 'st_country', true) ?></span></li>
				<li><span class="label"><?php echo __('Special Requirements', ST_TEXTDOMAIN); ?></span><span class="value"><?php echo get_post_meta($order_code, 'st_note', true) ?></span></li>
			</ul>
		</div>
        <?php
        $customer_infomation = @ob_get_clean();
        echo apply_filters( 'st_order_success_custommer_billing' , $customer_infomation, $order_code );
        ?>
        <div class="text-center mg20 mt30">
            <?php
            if (is_user_logged_in()){
                $page_user = st()->get_option('page_my_account_dashboard');
                if ($link = get_permalink($page_user)){
                    $link = esc_url(add_query_arg(array('sc'=>'booking-history'),$link));
                    ?>
                    <a href="<?php echo esc_url($link)?>" class="btn btn-primary"><i
                                class="fa fa-book"></i> <?php echo __('Booking Management' , ST_TEXTDOMAIN) ;  ?></a>
                    <?php
                }
            }
            ?>
            <?php
            $option_allow_guest_booking = st()->get_option('is_guest_booking');
            if($option_allow_guest_booking == 'on'){
                do_action('st_after_order_success_page_information_table',$order_code);
            }else{
                if (is_user_logged_in()){
                    do_action('st_after_order_success_page_information_table',$order_code);
                }
            }
            ?>

        </div>
	</div>
</div>
