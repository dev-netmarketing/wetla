<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

$is_localhost = ($_SERVER['REMOTE_ADDR'] == "127.0.0.1" or $_SERVER['REMOTE_ADDR'] == "::1");
if ( $is_localhost ):
  define( 'DB_NAME', 'wetla_database' );
  define( 'DB_USER', 'root' );
  define( 'DB_PASSWORD', '' );
  define( 'DB_HOST', 'localhost' );
  define( 'DB_CHARSET', 'utf8mb4' );
  define( 'DB_COLLATE', '');
  define( 'WP_DEBUG', true);
  define( 'WP_DEBUG_DISPLAY', false );
  define( 'SCRIPT_DEBUG', true );
  define( 'SAVEQUERIES', true );
  define( 'WP_DEBUG_LOG', true );
  define( 'WP_HOME', 'http://wetla.flamanttours.test' );
  define( 'WP_SITEURL', 'http://wetla.flamanttours.test' );
else:
  define( 'DB_NAME', 'wetla' );
  define( 'DB_USER', 'wetla' );
  define( 'DB_PASSWORD', 'wetla2021' );
  define( 'DB_HOST', 'localhost' );
  define( 'DB_CHARSET', 'utf8mb4' );
  define( 'DB_COLLATE', '');
  define( 'WP_DEBUG', false);
  define( 'WP_HOME', 'http://wetla.webdatasolutions.net' );
  define( 'WP_SITEURL', 'http://wetla.webdatasolutions.net' );
endif;


/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'rq1+2C!_l$FuT_, (DFLG,`Te.ORBz6R.Z9g_[O2-yA2_y4RvuHLIevkXonwUoYt' );
define( 'SECURE_AUTH_KEY',  '.q!hwYD6j.cSkwLluYAx9g2m^#L=3@OG[~hx0!WWml16C;zECl8yE=%_Uc4Z:]8T' );
define( 'LOGGED_IN_KEY',    '`=7`/2CRJQjGn0G3xj8q2)E:VJAa.NH, +RnV&l$3#g`cww9x955@)^NKBQPNX82' );
define( 'NONCE_KEY',        'd5JU?d.[,?Lu-FzJ^]@W[{UEZTqNB)5 {oU8@jiCy-F;YW`48b|T7t$FBbhn<f{@' );
define( 'AUTH_SALT',        'V}wHzpsI&QB,t0QUt1Tr&~kL,U44g<yo;)N!DnatUV7ymD w=s05mb2s^q?VSXnU' );
define( 'SECURE_AUTH_SALT', '~%F7CL8dDK.-fUkX-Lk`&Zh8tDh]tz =<ayb rfHIWc)aFFVkekK|U`W}bulBbi5' );
define( 'LOGGED_IN_SALT',   'zM4j8(Zl7iVw]o|c,j21/{S((AwECwyWm#<<4{+Dbl,HF* IC{krH,-#f40A.#]g' );
define( 'NONCE_SALT',       'V9AARCvjIAS1gDJG/3h.Qmy]C+FA0~P%^c>uLcPREDx-]|`-C%x<pcFAY>6@d>oF' );
/**#@-*/




/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_MEMORY_LIMIT', '256M' );


/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
